# Ligma Scraper CLI

## Overview
A command line interface version of a tool of the same name that used to be on my website. It connects to https://www.sigmaaldrich.com allowing you to search for a chemical and pull safety- and other relefant information off their site, in order to make labelling chemicals in the hobby lab less tedious.

## How to get
### Build from source
#### Preparation
- Make sure you have [go 1.19](https://golang.org) (or newer) available
- `cd` to the projects root directory
- Run `go get ./...` to ensure all the dependencies are available

#### Building
##### Quick way
- Just run `go run main.go` - this will compile it followed by running it in interactive mode
##### Proper way
- Run `go build -o bin/lscraper main.go` on a linux machine or `go build -o bin/lscraper.exe main.go` on a windows machine.
- Either put the binary you just compiled somewhere better where its being able to be located by $PATH or add its location to the variable
- run it

#### Grab a binary
This repo is set up with automatic CI, that will build binaries every commit, you can grab them from https://gitlab.com/ligmascraper/ligmascraper-cli/-/artifacts

## Authors
Conceived and built by Holger M Mürk



