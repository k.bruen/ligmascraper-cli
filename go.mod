module scraperCLI

go 1.19

require (
	github.com/antchfx/htmlquery v1.3.0
	github.com/manifoldco/promptui v0.9.0
	github.com/tidwall/gjson v1.17.0
	gopkg.in/h2non/gentleman.v2 v2.0.5
)

require (
	github.com/antchfx/xpath v1.2.3 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/nbio/st v0.0.0-20140626010706-e9e8d9816f32 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
