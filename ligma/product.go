package ligma

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"github.com/tidwall/gjson"
	"gopkg.in/h2non/gentleman.v2"
	"html"
	"log"
	"scraperCLI/ghs"
	"strings"
)

type ChemData struct {
	Name        string
	CAS         string
	MW          string
	Mp          string
	Bp          string
	Signalword  string
	Pictograms  []string
	Hcodes      []string
	Hstatements string
	Pcodes      []string
	Pstatements string
}

func Scrape(url string) (output ChemData) {
	cli := gentleman.New()
	cli.URL(url)
	req := cli.Request()
	req.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0")

	req.Method("GET")

	res, err := req.Send()

	if err != nil {
		return
	}
	doc, err := htmlquery.Parse(res.RawResponse.Body)
	if err != nil {
		log.Println(err.Error())
		return
	}

	node, err := htmlquery.Query(doc, "//script[@id='__NEXT_DATA__']")
	if err != nil {
		log.Println(err.Error())
		return
	}

	json := htmlquery.InnerText(node)

	parsedJSON := gjson.Parse(json)

	output = ChemData{
		Name:       parsedJSON.Get("props.pageProps.data.getProductDetail.name").String(),
		CAS:        parsedJSON.Get("props.pageProps.data.getProductDetail.casNumber").String(),
		MW:         parsedJSON.Get("props.pageProps.data.getProductDetail.molecularWeight").String(),
		Mp:         parsedJSON.Get("props.pageProps.data.getProductDetail.attributes.#(label==\"mp\").values.0").String(),
		Bp:         parsedJSON.Get("props.pageProps.data.getProductDetail.attributes.#(label==\"bp\").values.0").String(),
		Signalword: parsedJSON.Get("props.pageProps.data.getProductDetail.compliance.#(key==\"signalword\").value").String(),
		Pictograms: strings.Split(
			parsedJSON.Get("props.pageProps.data.getProductDetail.compliance.#(key==\"pictograms\").value").String(),
			",",
		),
		Hcodes: ghs.ParseHCodes(parsedJSON.Get("props.pageProps.data.getProductDetail.compliance.#(key==\"hcodes\").value").String()),
		Pcodes: ghs.ParsePCodes(parsedJSON.Get("props.pageProps.data.getProductDetail.compliance.#(key==\"pcodes\").value").String()),
	}

	output.Hstatements = ghs.DecodeHWords(output.Hcodes)
	output.Pstatements = ghs.DecodePWords(output.Pcodes)
	return
}

func (a ChemData) PrintName() {
	fmt.Println(html.UnescapeString(a.Name))
}

func (a ChemData) PrintPictograms() {
	for _, p := range a.Pictograms {
		fmt.Printf("[%s]\n", ghs.Pictograms[p])
	}
	fmt.Println()
}

func (a ChemData) PrintHStatements() {
	fmt.Println(html.UnescapeString(a.Hstatements))
}
func (a ChemData) PrintPStatements() {
	fmt.Println(html.UnescapeString(a.Pstatements))
}
func (a ChemData) PrintConsolidatedHP() {
	fmt.Println(html.UnescapeString(a.Signalword + " - " + a.Hstatements + " " + a.Pstatements))
}

func (a ChemData) PrintParams() {
	fmt.Println("CAS:           " + html.UnescapeString(a.CAS))
	fmt.Println("MW:            " + html.UnescapeString(a.MW))
	fmt.Println("mp:            " + html.UnescapeString(a.Mp))
	fmt.Println("bp:            " + html.UnescapeString(a.Bp))
	fmt.Println("Signal word:   " + html.UnescapeString(a.Signalword))
}
