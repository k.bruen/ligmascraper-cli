package ligma

import (
	"fmt"
	"github.com/tidwall/gjson"
	"gopkg.in/h2non/gentleman.v2"
	"strings"
)

type SearchResult struct {
	SearchQuery string     `json:"searchQuery"`
	SetsCount   int64      `json:"setsCount"`
	Chemicals   []Chemical `json:"chemicals"`
}
type Chemical struct {
	Name     string    `json: "name"`
	Products []Product `json: "products"`
}

type Product struct {
	Name       string `json: "name"`
	Url        string `json: "url"`
	ProductKey string `json: "productKey"`
}

func Search(query string) (output SearchResult) {
	output.SearchQuery = query

	cli := gentleman.New()
	cli.URL("https://www.sigmaaldrich.com/api")
	req := cli.Request()
	req.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0")

	req.SetHeader("Host", " www.sigmaaldrich.com")
	req.SetHeader("Accept", " */*")
	req.SetHeader("Accept-Language", "en-US,en;q=0.5")
	req.SetHeader("Accept-Encoding", "none")
	req.SetHeader("Content-Type", "application/json")
	req.SetHeader("Referer", "https://www.sigmaaldrich.com/EE/en/search/")
	req.SetHeader("x-gql-country", "EE")
	req.SetHeader("x-gql-language", "en")

	req.BodyString(
		fmt.Sprintf(
			"{\"operationName\":\"ProductSearch\",\"variables\":{\"searchTerm\":\"%s\",\"page\":1,\"group\":\"substance\",\"selectedFacets\":[],\"sort\":\"relevance\",\"type\":\"PRODUCT\"},\"query\":\"query ProductSearch($searchTerm: String, $page: Int!, $sort: Sort, $group: ProductSearchGroup, $selectedFacets: [FacetInput!], $type: ProductSearchType, $catalogType: CatalogType, $orgId: String, $region: String, $facetSet: [String], $filter: String) {\\n  getProductSearchResults(input: {searchTerm: $searchTerm, pagination: {page: $page}, sort: $sort, group: $group, facets: $selectedFacets, type: $type, catalogType: $catalogType, orgId: $orgId, region: $region, facetSet: $facetSet, filter: $filter}) {\\n    ...ProductSearchFields\\n    __typename\\n  }\\n}\\n\\nfragment ProductSearchFields on ProductSearchResults {\\n  metadata {\\n    itemCount\\n    setsCount\\n    page\\n    perPage\\n    numPages\\n    redirect\\n    __typename\\n  }\\n  items {\\n    ... on Substance {\\n      ...SubstanceFields\\n      __typename\\n    }\\n    ... on Product {\\n      ...SubstanceProductFields\\n      __typename\\n    }\\n    __typename\\n  }\\n  facets {\\n    key\\n    numToDisplay\\n    isHidden\\n    isCollapsed\\n    multiSelect\\n    prefix\\n    options {\\n      value\\n      count\\n      __typename\\n    }\\n    __typename\\n  }\\n  didYouMeanTerms {\\n    term\\n    count\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment SubstanceFields on Substance {\\n  _id\\n  id\\n  name\\n  synonyms\\n  empiricalFormula\\n  linearFormula\\n  molecularWeight\\n  aliases {\\n    key\\n    label\\n    value\\n    __typename\\n  }\\n  images {\\n    sequence\\n    altText\\n    smallUrl\\n    mediumUrl\\n    largeUrl\\n    brandKey\\n    productKey\\n    label\\n    videoUrl\\n    __typename\\n  }\\n  casNumber\\n  products {\\n    ...SubstanceProductFields\\n    __typename\\n  }\\n  match_fields\\n  __typename\\n}\\n\\nfragment SubstanceProductFields on Product {\\n  name\\n  displaySellerName\\n  productNumber\\n  productKey\\n  isSial\\n  isMarketplace\\n  marketplaceSellerId\\n  marketplaceOfferId\\n  cardCategory\\n  cardAttribute {\\n    citationCount\\n    application\\n    __typename\\n  }\\n  substance {\\n    id\\n    __typename\\n  }\\n  casNumber\\n  attributes {\\n    key\\n    label\\n    values\\n    __typename\\n  }\\n  speciesReactivity\\n  brand {\\n    key\\n    erpKey\\n    name\\n    color\\n    __typename\\n  }\\n  images {\\n    altText\\n    smallUrl\\n    mediumUrl\\n    largeUrl\\n    __typename\\n  }\\n  linearFormula\\n  molecularWeight\\n  description\\n  sdsLanguages\\n  sdsPnoKey\\n  similarity\\n  paMessage\\n  features\\n  catalogId\\n  materialIds\\n  erp_type\\n  __typename\\n}\\n\"}",
			query,
		),
	)

	req.Method("POST")

	res, err := req.Send()

	if err != nil {
		return
	}

	if err != nil {
		return
	}

	responseJSON := gjson.ParseBytes(res.Bytes())

	output.SetsCount = responseJSON.Get("data.getProductSearchResults.metadata.setsCount").Int()

	output.Chemicals = []Chemical{}

	items := responseJSON.Get("data.getProductSearchResults.items").Array()
	for _, row := range items {
		var chem = Chemical{}
		chem.Name = row.Get("name").String()

		cells := row.Get("products").Array()
		chem.Products = []Product{}

		for _, cell := range cells {
			var product Product
			product.Name = cell.Get("name").String()
			product.ProductKey = cell.Get("productKey").String()
			product.Url = fmt.Sprintf(
				"https://www.sigmaaldrich.com/EE/en/product/%s/%s",
				strings.ToLower(cell.Get("brand.key").String()),
				product.ProductKey,
			)

			chem.Products = append(chem.Products, product)
		}

		output.Chemicals = append(output.Chemicals, chem)
	}

	return
}
