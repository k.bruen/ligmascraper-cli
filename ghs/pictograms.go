package ghs

var Pictograms = map[string]string{
	"GHS01": "GHS01 - Health Hazard",
	"GHS02": "GHS02 - Flammable",
	"GHS03": "GHS03 - Exclamation Mark",
	"GHS04": "GHS04 - Pressured Gas",
	"GHS05": "GHS05 - Corrosive",
	"GHS06": "GHS06 - Explosion Hazard",
	"GHS07": "GHS07 - Oxidizer",
	"GHS08": "GHS08 - Environmental Hazard",
	"GHS09": "GHS09 - Toxic",
}
