package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/manifoldco/promptui"
	"os"
	"scraperCLI/ligma"
	"strings"
	"sync"
)

var jsonOutput = flag.Bool("j", false, "Print output as a JSON file")
var searchMode = flag.Bool("S", false, "Search for a string")
var scrapeMode = flag.Bool("c", false, "Scrape a provided URL")

func main() {
	flag.Parse()

	if *searchMode {
		if len(os.Args) == 1 {
			fmt.Println("You need to provide a query!")
			return
		}
		performSearch()
	} else if *scrapeMode {
		if len(os.Args) == 1 {
			fmt.Println("You need to provide a target URL!")
			return
		}
		performScrape()
	} else {
		interActiveMode()
	}

}

func interActiveMode() {
	query := promptSearchQuery()

	searchResults := ligma.Search(query)

	fmt.Sprintf("%d results for \"%s\"", searchResults.SetsCount, searchResults.SearchQuery)

	chemical := selectChemical(searchResults.Chemicals)

	product := selectProduct(chemical)

	chemData := ligma.Scrape(product.Url)

	printResults(chemData)
}

func selectProduct(chem ligma.Chemical) ligma.Product {
	menuItems := []string{}
	productMap := make(map[string]ligma.Product)

	for _, p := range chem.Products {
		kn := p.ProductKey + " - " + p.Name
		menuItems = append(menuItems, kn)
		productMap[kn] = p
	}

	prompt := promptui.Select{
		Label: "Select product",
		Items: menuItems,
	}

	_, result, err := prompt.Run()

	fmt.Println(result)

	if err != nil {
		panic(err.Error())
	}

	return productMap[result]
}

func selectChemical(chems []ligma.Chemical) (choice ligma.Chemical) {
	menuItems := []string{}
	chemMap := make(map[string]ligma.Chemical)

	for _, chem := range chems {
		menuItems = append(menuItems, chem.Name)
		chemMap[chem.Name] = chem
	}

	prompt := promptui.Select{
		Label: "Select Day",
		Items: menuItems,
	}

	_, result, err := prompt.Run()

	fmt.Println(result)

	if err != nil {
		panic(err.Error())
	}

	return chemMap[result]
}

func promptSearchQuery() (query string) {
	prompt := promptui.Prompt{
		Label: "Search Sigma",
		Validate: func(s string) error {
			if len(s) == 0 {
				return errors.New("Enter a query!")
			}
			return nil
		},
	}

	query, err := prompt.Run()

	if err != nil {
		panic(err)
	}

	return query
}

func performSearch() {
	searchResult := ligma.Search(strings.Join(flag.Args(), " "))

	if *jsonOutput {
		jsonString, err := json.Marshal(searchResult)
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			panic(err)
		}
		fmt.Println(string(jsonString))
	} else {
		fmt.Println()
		fmt.Println(searchResult)
	}
}

func performScrape() {
	results := make([]ligma.ChemData, 0)

	var wg sync.WaitGroup

	for _, url := range flag.Args() {
		wg.Add(1)
		go func() {
			results = append(results, ligma.Scrape(url))
			wg.Done()
		}()
	}

	wg.Wait()

	if *jsonOutput {
		jsonString, _ := json.Marshal(results)
		fmt.Print(string(jsonString))
		return
	}

	for _, chem := range results {
		printResults(chem)
	}
}

func printResults(booty ligma.ChemData) {
	booty.PrintName()
	fmt.Println()
	booty.PrintPictograms()
	booty.PrintParams()
	fmt.Println()
	booty.PrintHStatements()
	fmt.Println()
	booty.PrintPStatements()
	fmt.Println()
	booty.PrintConsolidatedHP()
}
